export default {
  title: (title) => {
    if (!title || title === 'Without title') {
      return 'Title is required';
    }

    if (title.length > 30) {
      return 'Title must be less than 30 characters';
    }

    if (title.length < 3) {
      return 'Title must have at least 3 caracteres';
    }

    return false;
  },

  date: (date) => {
    if (!date) {
      return 'Date is required';
    }

    return false;
  },

  items: (items) => {
    if (items.length < 1) {
      return 'Insert a item';
    }

    return false;
  },

  item: (item) => {
    if (item.title.length > 30) {
      return 'Item must be less than 30 characters';
    }

    if (item.title.length < 3) {
      return 'Item must have at least 3 caracteres';
    }

    return false;
  }
};

import notes from './notes';
import items from './items';

export default {
  ...notes,
  ...items,

  mount(state, storageState) {
    state.notes = storageState.notes;
  }
};

export default {
  notes: ({ notes }) => notes,
  noteById: ({ notes }) => (id) => notes.find((n) => n.id === id)
};

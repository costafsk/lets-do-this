export default {
  sync(state) {
    localStorage.removeItem('state');
    localStorage.setItem('state', JSON.stringify(state));
  }
};

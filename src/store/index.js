import Vue from 'vue';
import Vuex from 'vuex';

import modules from './modules';

Vue.use(Vuex);

const { Store } = Vuex;

export default new Store(modules);

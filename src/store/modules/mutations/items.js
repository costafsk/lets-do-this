import Helpers from '../../../helpers';

const items = {
  changeSelectItem(state, data) {
    const { noteId, itemId, checked } = data;

    state.notes = state.notes.map((note) => {
      if (note.id === noteId) {
        note.items = note.items.map((item) => {
          if (item.id === itemId) {
            item.checked = checked;
          } return item;
        });
      } return note;
    });

    Helpers.vuex.sync(state);
  }
};

export default items;

export default {
  saveNote: ({ commit }, note) => commit('saveNote', note),

  removeNotes: ({ commit }, line) => commit('removeNotes', line)
};

export default {
  createId: (name = 'without name') => {
    if (typeof name !== 'string' || !name) name = 'without name';

    name = name.trim();

    return `${name.replace(/( )+/g, '-')}-${new Date().getTime()}`;
  }
};

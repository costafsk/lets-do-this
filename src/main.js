import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import vuetify from './plugins/vuetify';

import './scss/master.scss';

import store from './store';

import Dashboard from './pages/Dashboard/index.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', component: Dashboard }
];

const router = new VueRouter({
  routes
});

Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  store,
  render: (h) => h(App)
}).$mount('#app');

import notes from './notes';
import items from './items';

export default {
  ...notes,
  ...items,

  mount: ({ commit }, storageState) => commit('mount', storageState)
};

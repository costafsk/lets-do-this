import vuex from './vuex';
import identify from './identify';

export default {
  vuex,
  identify
};

import Helpers from '../../../helpers';

const notes = {
  saveNote(state, note) {
    const search = state.notes.find((i) => i.id === note.id);

    if (!search) {
      state.notes.push(note);
      Helpers.vuex.sync(state);
    } else {
      const index = state.notes.indexOf(search);

      if (index > -1) {
        state.notes = state.notes.map((n) => {
          if (n.id === note.id) return note;

          return n;
        });
      }
    }

    Helpers.vuex.sync(state);
  },

  async removeNotes(state, line) {
    line.map((id) => {
      const search = state.notes.find((i) => i.id === id);

      if (search) {
        const index = state.notes.indexOf(search);

        if (index > -1) {
          state.notes.splice(index, 1);
          Helpers.vuex.sync(state);
        }
      }

      return id;
    });
  },
};

export default notes;

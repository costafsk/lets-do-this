module.exports = {
  "env": {
    "browser": true,
  },
  "extends": [
    "plugin:vue/essential",
    "airbnb-base"
  ],
  "parserOptions": {
    "ecmaVersion": 2018,
    "sourceType": "module"
  },
  "plugins": [
    "vue"
  ],
  "rules": {
    "indent": ["error", 2],
    "no-alert": "error",
    "prefer-const": "error",
    "restricted-syntax": "off",
    "eqeqeq": "error",
    "camelcase": "error",
    "max-depth": ["warn", 4],
    "max-params": ["warn", 4],
    "max-statements": ["warn", 20],
    "no-param-reassign": "off",
    "require-jsdoc": "off",
    "comma-dangle": "off",
  }
};